class TerminalConfiguration:
    @staticmethod
    def Start():
        clear()
        print("Config:\nOS        DISKS     BOOT      CLOSE")
        mode = input()
        prefix = ""
        run = True
        while run:
            clear()
            mode = mode.upper()
            if mode == "":
                print("Config:\nOS        DISKS     BOOT      CLOSE")
                prefix = ""
            elif mode == "OS":
                print("CONFIG:\nOS\n0.PRE\n1.USERS\n2.APPS")
                prefix = "OS"
            elif mode == "DISKS":
                print("CONFIG:\nDISKS\nADD       REMOVE    EDIT      READ")
                i = 0
                for x in os_disk:
                    text = str(i) + ""
                    while len(text) < 3:
                        text = text + " "
                    text = text + " " + str(x[1])
                    print(text)
                    i += 1
                prefix = "DISKS"
            elif mode == "BOOT":
                print("Config:\nBOOT\n0.Login\n1.VIEW")
                prefix = "BOOT"
            # OS
            # OS>USERS
            elif mode == "OSUSERS":
                print("CONFIG:\nOS>USERS\nADD       REMOVE    PRE")
                i = 0
                for user in os_users:
                    print(str(i) + "." + user[0])
                    i += 1
                prefix = "OSUSERS"
            elif mode == "OSUSERSADD":
                print("CONFIG:\nOS>USERS>ADD")
                username = input("Username:")
                password = input("Password:")
                OS.AddUser(username, password)
                mode = "OSUSERS"
                continue
            elif mode == "OSUSERSREMOVE":
                print("CONFIG:\nOS>USERS>REMOVE")
                username = input("Username:")
                password = input("Password:")
                OS.RemoveUser(username, password)
                mode = "OSUSERS"
                continue
            elif mode == "OSUSERSPRE":
                mode = "OS"
                continue
            # /OS>USERS
            # OS>APPS
            elif mode == "OSAPPS":
                print("CONFIG:\nOS>APPS\nADD       REMOVE    PRE")
                i = 0
                for x in os_apps:
                    text = str(i) + "." + x[0]
                    while len(text) < 32:
                        text = text + " "
                    text = text + " " + x[1]
                    print(text)
                    i += 1
                prefix = "OSAPPS"
            elif mode == "OSAPPSADD":
                print("CONFIG:\nOS>APPS>ADD")
                name = input("App name:")
                tag = input("App tag:")
                OS.AddApp(name, tag)
                mode = "OSAPPS"
                continue
            elif mode == "OSAPPSREMOVE":
                print("CONFIG:\nOS>APPS>REMOVE")
                name = input("App name:")
                tag = input("App tag:")
                OS.RemoveApp(name, tag)
                mode = "OSAPPS"
                continue
            elif mode == "OSAPPSPRE":
                mode = "OS"
                continue
            # /OS>APPS
            # /OS
            # DISKS
            elif mode == "DISKSADD":
                memory = int(input("Memory:"))
                pos = len(os_disk)
                AddDisk(pos, memory)
                mode = "DISKS"
                continue
            elif mode == "DISKSREMOVE":
                pos = int(input("Pos:"))
                memory = int(input("Memory:"))
                RemoveDisk(pos, memory)
                mode = "DISKS"
                continue
            elif mode == "DISKSEDIT":
                method = int(input("cluster-0 Byte-1 "))
                if method == 0:
                    pos = int(input("Pos:"))
                    memory = int(input("Memory:"))
                    cluster = int(input("Cluster:"))
                    data = input("Data:")
                    EditDisk(pos, memory, cluster, data)
                elif method == 1:
                    pos = int(input("Pos:"))
                    memory = int(input("Memory:"))
                    cluster = int(input("cluster:"))
                    byte = int(input("Byte:"))
                    data = input("Data:")
                    EditDiskB(pos, memory, cluster, byte, data)
                mode = "DISKS"
                continue
            elif mode == "DISKSREAD":
                method = int(input("cluster-0 Byte-1 "))
                if method == 0:
                    pos = int(input("Pos:"))
                    memory = int(input("Memory:"))
                    cluster = int(input("cluster:"))
                    print(ReadDisk(pos, memory, cluster))
                elif method == 1:
                    pos = int(input("Pos:"))
                    memory = int(input("Memory:"))
                    cluster = int(input("cluster:"))
                    byte = int(input("Byte:"))
                    print(ReadDiskB(pos, memory, cluster, byte))
                input()
                mode = "DISKS"
                continue
            # /DISKS
            # BOOT
            elif mode == "BOOTLOGIN":
                print("Config:\nBOOT>LOGIN\n0.PRE\n1.ENABLE\n2.AUTO")
                prefix = "BOOTLOGIN"
            elif mode == "BOOTLOGINENABLE":
                print("0-OFF 1-ON")
                EditDiskB(0, 512, 1, 5, CryptS(input()))
                mode = "BOOTLOGIN"
                continue
            elif mode == "BOOTLOGINAUTO":
                print("Config:\nBOOT>LOGIN>AUTO\n0.PRE\n1.ENABLE\n2.cluster\n3.Data")
                prefix = "BOOTLOGINAUTO"
            elif mode == "BOOTLOGINAUTOENABLE":
                print("0-OFF 1-ON")
                EditDiskB(0, 512, 1, 6, CryptS(input()))
                mode = "BOOTLOGINAUTO"
                continue
            elif mode == "BOOTLOGINAUTOcluster":
                EditDiskB(0, 512, 1, 7, CryptS(input("cluster with login&password:")))
                mode = "BOOTLOGINAUTO"
                continue
            elif mode == "BOOTLOGINAUTODATA":
                username = list(input("Username:"))
                password = list(input("Password:"))
                loginpass = int(DeCryptS(ReadDiskB(0, 512, 1, 7)))
                i = 0
                for x in username:
                    i += 1
                    char = CryptS(x)
                    EditDiskB(0, 512, loginpass, i, char)
                i += 1
                EditDiskB(0, 512, loginpass, i, CryptS(" "))
                for x in password:
                    i += 1
                    char = CryptS(x)
                    EditDiskB(0, 512, loginpass, i, char)
                i += 1
                EditDiskB(0, 512, loginpass, i, CryptS(" "))
                mode = "BOOTLOGINAUTO"
                continue
            elif mode == "BOOTLOGINAUTOPRE":
                mode = "BOOTLOGIN"
                prefix = "BOOTLOGIN"
                continue
            elif mode == "BOOTLOGINPRE":
                mode = "BOOT"
                prefix = "BOOT"
                continue
            elif mode == "BOOTVIEW":
                print("0-STANDART 1-CUSTOM")
                EditDiskB(0, 512, 1, 8, CryptS(input()))
                mode = "BOOT"
                continue
            # /BOOT
            elif mode == "OSPRE" or mode == "DISKSPRE" or mode == "BOOTPRE":
                mode = ""
                continue
            elif mode == "CLOSE":
                run = False
                break

            mode = prefix + input()
        clear()
        return None
