class Matrix:
    t = 0
    lines = []
    @staticmethod
    def Start():
        Matrix.lines = []
        print("During of matrix,if set 0 u need to restart vos")
        Matrix.t = int(input("During:"))
        for l in range(0, 21):
            Matrix.lines.append(Matrix.CreateLine())
        Matrix.Loop()
        return

    @staticmethod
    def CreateLine():
        from random import randint
        sybm = []
        char = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
                "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F",
                "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
        for c in range(0, 51):
            sybm.append(char[randint(0, len(char) - 1)])
        return "".join(sybm)

    @staticmethod
    def Loop():
        if Matrix.t == 0:
            while True:
                d = [""]
                for l in Matrix.lines:
                    d.append(l)
                d.remove(Matrix.lines[-1])
                d[0] = Matrix.CreateLine()
                Matrix.lines = d
                Matrix.Print()
                sleep(0.25)
        else:
            while Matrix.t > 0.0:
                d = [""]
                for l in Matrix.lines:
                    d.append(l)
                d.remove(Matrix.lines[-1])
                d[0] = Matrix.CreateLine()
                Matrix.lines = d
                Matrix.Print()
                Matrix.t -= 0.25
                sleep(0.25)
        return

    @staticmethod
    def Print():
        clear()
        for l in Matrix.lines:
            print(l)
        return
