class TerminalAppStore:
    @staticmethod
    def Start():
        TerminalAppStore.Install("Matrix")
        return

    @staticmethod
    def Install(name):
        import os
        if os.path.isfile(os.path.abspath("") + "\\apps\\" + name + ".py"):
            return
        else:
            from urllib import parse, request
            req = request.urlopen(
                "https://gitlab.com/talergrichina/py-vos-apps/raw/master/require/" + name + ".dat")
            lines = req.readlines()
            for l in lines:
                _d = l.decode("utf-8").split()
                _name = _d[0]
                _type = _d[1].replace("\n", "")
                if _type[0] == "c":
                    _type = "core"
                elif _type[0] == "m":
                    _type = "module"
                if os.path.isfile(os.path.abspath("") + "\\" + _type + "s\\" + _name):
                    continue
                else:
                    _req = request.urlopen(
                        "https://gitlab.com/talergrichina/py-vos-" + _type + "s/raw/master/" + _name)
                    _file = open(os.path.abspath("") + "\\" + _type + "s\\" + _name, "x")
                    _file.write(_req.read().decode("utf-8"))
                    _file.close()
            data = request.urlopen("https://gitlab.com/talergrichina/py-vos-apps/raw/master/" + name + ".py")
            file = open(os.path.abspath("") + "\\apps\\" + name + ".py", "x")
            file.write(data.read().decode("utf-8"))
            file.close()
            if type == "app":
                OS.AddApp(name, name)
        return
